// File that specifies every test case
describe('Protractor Pokemon App', function(host) {
	/*tip when not finding an element try to use code bellow*/
	//browser.waitForAngular();
	//browser.driver.sleep(5000);

	var buttonSelectPokemon = element.all(by.css('.list .item')).first();
	var name = 'protractor-test';
	var email = 'protractor-test@test.com';
	var password = 'password';
	var description_new = 'This an automated protractor test new comment.';
	var description_update = 'This an automated protractor test update comment.';
	var oakMessage = element(by.css('.disclaimer .next'));
	var oakMessagesLength = 6;

	beforeEach(function() {
		browser.get('');
	});


	it('should do introduction and create user', function() {
		for (var i = 0; i < oakMessagesLength; i++) {
			oakMessage.click();
		};

		//insert data on form
		element(by.css('.button.new')).click();
		element(by.model('vm.newUser.name')).sendKeys(name);
		element(by.model('vm.newUser.email')).sendKeys(email);
		element(by.model('vm.newUser.password')).sendKeys(password);

		element(by.css('.button.save')).click();
		browser.driver.sleep(5000);
	});

	it('should do introduction and login', function() {
		for (var i = 0; i < oakMessagesLength; i++) {
			oakMessage.click();
		};

		//insert data on form
		element(by.model('vm.newUser.name')).sendKeys(name);
		element(by.model('vm.newUser.password')).sendKeys(password);

		element(by.css('.button.save')).click();
		browser.driver.sleep(5000);
	});

	it('should update user data', function() {
		buttonSelectPokemon.click();
		element(by.css('.view')).click();
		browser.driver.sleep(5000);
		element(by.css('.menu')).click();
		element(by.css('.userdata')).click();
		browser.driver.sleep(5000);

		//insert data on form
		element(by.model('vm.newUser.email')).clear().sendKeys(email);
		element(by.model('vm.newUser.password')).clear().sendKeys(password);

		element(by.css('.button.save')).click();
		browser.driver.sleep(5000);
	});

	it('should have at least one pokemon', function() {
		expect(element.all(by.css('.list .item span')).first().getText()).toEqual('bulbasaur');
	});

	it('should have at least one pokemon data', function() {
		buttonSelectPokemon.click();
		expect(element.all(by.css('.pokemon .info .status')).first().getText()).toEqual('No. 1');
	});

	it('test adding a comment', function() {
		//find element
		buttonSelectPokemon.click();
		element(by.css('.view')).click();
		browser.driver.sleep(5000);
		element(by.css('.menu')).click();
		element(by.css('.show')).click();
		browser.driver.sleep(5000);
		//insert data on form
		element(by.model('vm.newComment.description')).sendKeys(description_new);

		//save data
		element(by.css('.button.save')).click();
		browser.driver.sleep(5000);

		//look for new data
		var elementCreated = element.all(by.css('.row .description')).first();
		expect(elementCreated.getText()).toEqual(description_new);
	});

	it('test updating a comment', function() {
		//find element
		buttonSelectPokemon.click();
		element(by.css('.view')).click();
		browser.driver.sleep(5000);
		var link = element.all(by.css(".row")).first();
		browser.actions().mouseMove(link).perform();
		element.all(by.css('.edit')).first().click();
		browser.driver.sleep(5000);

		//save data
		element(by.model('vm.newComment.description')).clear().sendKeys(description_update);
		element(by.css('.button.save')).click();
		browser.driver.sleep(5000);

		//look for updated data
		var elementCreated = element.all(by.css('.row .description')).first();
		expect(elementCreated.getText()).toEqual(description_update);
	});

	it('test removing a comment', function() {
		//find element
		buttonSelectPokemon.click();
		element(by.css('.view')).click();
		browser.driver.sleep(5000);
		var link = element.all(by.css(".row")).first();
		browser.actions().mouseMove(link).perform();
		element.all(by.css('.remove')).first().click();
		browser.driver.sleep(5000);
	});

});
