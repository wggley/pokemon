// conf.js
exports.config = {
    framework: 'jasmine2',
    baseUrl: 'http://localhost/pokemon/app',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['spec.js'],
    capabilities: {
        browserName: 'chrome'
    }
}
