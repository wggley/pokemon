// conf.js
exports.config = {
    framework: 'jasmine2',
    baseUrl: 'http://pokedex.parseapp.com',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['spec.js'],
    capabilities: {
        browserName: 'chrome'
    }
}
