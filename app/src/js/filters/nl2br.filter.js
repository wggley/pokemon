/*filter line breaks to br html tags*/
(function() {
    angular
        .module('pokedexApp')
        .filter('nl2br', nl2br);

    nl2br.$inject = [];

    function nl2br() {
        return function(text) {
            return text ? text.replace(/\n/g, '<br/>') : '';
        }
    };
})();
