/**
 * App States configurations
 */
(function() {
    angular
        .module('pokedexApp')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    /**
     * Provide states for app
     * @param  {object}
     * @param  {object}
     */
    function config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise(function($injector, $location) {
            var $state = $injector.get("$state");
            $state.go("pokedex");
        });

        $stateProvider
            .state('intro', {
                url: "/intro",
                controller: 'introController as vm',
                templateUrl: 'src/views/intro.view.html',
            })
            .state('pokedex', {
                url: "/pokedex",
                controller: 'pokedexController as vm',
                templateUrl: 'src/views/pokedex.view.html',
                data: {
                    rule: verifyUserLogged
                }
            })
            .state('pokemon', {
                url: "/pokemon/national_id/:national_id",
                controller: 'pokemonController as vm',
                templateUrl: 'src/views/pokemon.view.html',
                data: {
                    rule: verifyUserLogged
                }
            })
            .state('comments', {
                url: "/pokemon/national_id/:national_id/comments",
                controller: 'commentsController as vm',
                templateUrl: 'src/views/comments.view.html',
                data: {
                    rule: verifyUserLogged
                }
            })
            .state('comment', {
                url: "/pokemon/national_id/:national_id/comment/id/:id",
                controller: 'commentController as vm',
                templateUrl: 'src/views/comment.view.html',
                data: {
                    rule: verifyUserLogged
                }
            })
            .state('user', {
                url: "/user",
                controller: 'userController as vm',
                templateUrl: 'src/views/user.view.html',
            });

        /**
         * Check user is logged
         * @return {boolean} true if user is logged
         */
        function verifyUserLogged() {
            var currentUser = Parse.User.current();
            if (currentUser) {
                return false;
            } else {
                return {
                    toState: "intro"
                };
            }
        }
    }

    angular
        .module('pokedexApp')
        .run(run);

    run.$inject = ['$rootScope', '$state'];

    /**
     * Run function
     * @param  {object} $rootScope angular $rootScope object
     * @param  {object} $state     angular $state object
     */
    function run($rootScope, $state) {
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (!angular.isObject(toState.data) || !angular.isFunction(toState.data.rule)) {
                return;
            }
            var result = toState.data.rule();

            if (result && result.toState) {
                event.preventDefault();
                $state.go(result.toState, result.params);
            }
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            //assign the "from" parameter to something
            toParams.backState = fromState.name;
            toParams.backStateParams = fromParams;
        });
    };

})();
