/**
 * App initialization
 */
(function() {
    var injection = ['ngAnimate', 'ui.router', 'angularSpinners', 'ngSanitize'];

    angular.module('pokedexApp', injection);

    /**
     * Parse initialization
     */
    Parse.initialize("0W4c9tB6nUMn7jFRFIvFFmmUOAuRSANVkZjeXfkJ", "lQdS3KXoGq4PwVJI1rqDeROyo84yYoP0RQyDfD80");
})();
