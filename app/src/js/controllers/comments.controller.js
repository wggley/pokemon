/**
 * Comments Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('commentsController', commentsController);

    commentsController.$inject = ['$scope', '$state', 'commentsService', 'commentService'];

    /**
     * Comments Controller Function
     * @param  {object} $scope          angular $scope object
     * @param  {object} $state          angular $state object
     * @param  {object} commentsService Comments Service
     * @param  {object} commentService Comment Service
     */
    function commentsController($scope, $state, commentsService, commentService) {
        var vm = this;
        vm.browser = browser;
        vm.nationalId = $state.params.national_id;
        vm.menuVisible = false;

        // public functions
        vm.doLogout = doLogout;
        vm.getCommentDate = getCommentDate;
        vm.gotoUserData = gotoUserData;
        vm.hideComments = hideComments;
        vm.removeComment = removeComment;
        vm.showCreateComment = showCreateComment;
        vm.showEditionComment = showEditionComment;
        vm.verifyUserCanEditOrRemove = verifyUserCanEditOrRemove;


        // Controller init
        getComments();

        ///////

        /**
         * Do Logout
         */
        function doLogout() {
            Parse.User.logOut();
            $state.go('intro');
        }

        /**
         * Get all comments for a pokemon
         */
        function getComments() {
            if (typeof(vm.nationalId) == "undefined" || vm.nationalId == "") {
                return hideComments();
            }
            commentsService.getComments(renderComments, vm.nationalId);
        }

        /**
         * Get User Data
         * @return {boolean}
         */
        function gotoUserData() {
            $state.go('user');
        }

        /**
         * Get Comment Date to be displayed
         * @param  {object} comment Comments Service comment object
         * @return {string}         Date to be displayed
         */
        function getCommentDate(comment) {
            return comment.get('date_updated') || comment.get('date_created');
        }

        /**
         * Hide All Comments
         */
        function hideComments() {
            $state.go('pokemon', {
                national_id: vm.nationalId
            });
        }

        /**
         * Display New Comment Form
         */
        function showCreateComment() {
            $state.go('comment', {
                national_id: vm.nationalId,
                id: ""
            });
        }

        /**
         * Edit comment
         * @param  {object} comment Comments Service comment object
         */
        function showEditionComment(comment) {
            if (verifyUserCanEditOrRemove(comment)) {
                $state.go('comment', {
                    national_id: vm.nationalId,
                    id: comment.id
                });
            }
        }

        /**
         * Remove comment
         * @param  {object} comment Comments Service comment object
         */
        function removeComment(comment) {
            commentService.removeComment(getComments, comment);
        }

        /**
         * Render comments
         * @param  {object} comments Comments Service comment object
         */
        function renderComments(comments) {
            vm.comments = [];
            if (comments.length > 0) {
                vm.comments = comments;
            }
            $scope.$apply();
        }

        /**
         * Verify user can edit/remove a comment
         * @param  {object} comment Comments Service comment object
         */
        function verifyUserCanEditOrRemove(comment) {
            var commentUser = comment.get('user');
            var currentUser = Parse.User.current();
            if (currentUser) {
                return commentUser.id == currentUser.id;
            }
            return false;
        }
    };
})();
