/**
 * Intro Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('introController', introController);

    introController.$inject = ['$state'];

    /**
     * Intro Controller function
     * @param  {object} $state          angular $state object
     */
    function introController($state) {
        var vm = this;
        vm.editing = false;
        vm.id = $state.params.id;
        vm.currentMessage = 1;

        /*public functions*/
        vm.displayMessage = displayMessage;
        vm.getNextMessage = getNextMessage;

        // controller init
        // none yet
        ///////

        /**
         * Verify if needs to display current Message
         * @param  {integer} element chosen element
         * @return {boolean}         true = show, false = hide
         */
        function displayMessage(element) {
            return element == vm.currentMessage;
        }

        /**
         * Set next message to display
         */
        function getNextMessage() {
            vm.currentMessage++;
            if (vm.currentMessage > 6) {
                $state.go('user');
            }
        }
    };
})();
