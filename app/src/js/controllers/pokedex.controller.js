/**
 * Pokedex Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('pokedexController', pokedexController);

    pokedexController.$inject = ['$state', 'pokedexService', 'pokemonService'];

    /**
     * Pokedex Controller function
     * @param  {object}
     * @param  {object}
     */
    function pokedexController($state, pokedexService, pokemonService) {
        var vm = this;

        //public functions
        vm.getPokemon = getPokemon;

        //init controller
        getPokedex();

        ///////

        /**
         * Get Pokedex Data
         */
        function getPokedex() {
            pokedexService.getPokedex(renderPokedex);
        }

        /**
         * Render Pokedex list
         * @param  {object}
         */
        function renderPokedex(data) {
            vm.pokedexData = data;
        }

        /**
         * Get Pokemon Data
         * @param  {object} pokemon Pokemon resource data
         */
        function getPokemon(pokemon, index) {
            pokemonService.setPokemon(pokemon, index);
            $state.go('pokemon', {
                national_id: pokemon.national_id
            });
        }

    };

})();
