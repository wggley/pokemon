/**
 * User Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('userController', userController);

    userController.$inject = ['$scope', '$state', 'userService'];

    /**
     * User Controller function
     * @param  {object} $scope          angular $scope object
     * @param  {object} $state          angular $state object
     * @param  {object} userService User Service
     */
    function userController($scope, $state, userService) {
        var vm = this;
        vm.formBehaviour = 'login'; // 'login', 'new', 'edit', 'disable'
        vm.id = $state.params.id;

        /*public functions*/
        vm.cancelCreateUser = cancelCreateUser;
        vm.createUser = createUser;
        vm.disableSubmitButton = disableSubmitButton;
        vm.getFormBehaviour = getFormBehaviour;
        vm.submitForm = submitForm;

        // controller init
        getUser();

        ///////

        /**
         * Cancel User edition
         * @param  {string} errorMessage
         */
        function cancelCreateUser(errorMessage) {
            if (vm.formBehaviour == "edit") {
                $scope.$emit('goBack');
            }
            vm.formBehaviour = 'login';
        }

        /**
         * Create User
         * @param  {string} errorMessage
         */
        function createUser(userForm) {
            vm.formBehaviour = "new";
            vm.newUser = {
                name: "",
                email: "",
                password: ""
            };
            userForm.$setPristine();
        }

        /**
         * Disable submit button
         * @param  {object} userForm angular form
         * @return {boolean}
         */
        function disableSubmitButton(userForm) {
            return userForm.$pristine || (userForm.$dirty && userForm.$invalid);
        }

        /**
         * Form Validation
         * @param  {boolean} valid form validation status
         * @return {boolean}       true = ok, false = errors
         */
        function formValidation(valid) {
            if (!valid || (vm.formBehaviour == "new" && vm.newUser.name == "") || vm.newUser.email == "" || vm.newUser.password == "") {
                return false;
            }
            return true;
        }

        /**
         * Verify form behaviour
         * @param  {string} status 'login', 'edit', 'disable'
         * @return {boolean}        true if status matches
         */
        function getFormBehaviour(status) {
            if (status == "disable" && vm.formBehaviour == "edit") {
                return true;
            }
            if (status == "disable" && vm.formBehaviour == "new") {
                return false;
            }
            if (status == "edit" && vm.formBehaviour == "new") {
                return true;
            }
            return vm.formBehaviour == status;
        }

        /**
         * Get User
         */
        function getUser() {
            var user = Parse.User.current();
            if (user) {
                vm.formBehaviour = 'edit';
                renderUser(user);
            }
        }

        /**
         * Render User
         * @param  {object} user User Service User object
         */
        function renderUser(user) {
            vm.user = user;
            if (typeof(user) == "undefined") {
                return cancelCreateUser();
            }

            vm.newUser = {
                name: user.get('username'),
                email: user.get('email'),
                password: user.get('password')
            };
            //$scope.$apply();
        }

        /**
         * Submit User Form
         * @param  {object} userForm angular form
         */
        function submitForm(userForm) {
            if (formValidation(userForm.$valid)) {
                if (vm.formBehaviour == "login") {
                    return userService.getUser(afterServiceCall, vm.newUser.name, vm.newUser.password);
                }
                if (vm.formBehaviour == "edit") {
                    return userService.updateUser(afterServiceCall, vm.newUser.email, vm.newUser.password);
                }
                if (vm.formBehaviour == "new") {
                    return userService.createUser(afterServiceCall, vm.newUser.name, vm.newUser.email, vm.newUser.password);
                }
            }
        }

        /**
         * Callback after Service Call
         * @param  {string} errorMessage service error message
         */
        function afterServiceCall(errorMessage) {
            if (typeof(errorMessage) == "string") {
                vm.errorMessage = errorMessage;
            } else if (vm.formBehaviour == "edit") {
                vm.errorMessage = "user updated with success.";
            } else {
                return $state.go('pokedex');
            }
            $scope.$apply();
        }
    };
})();
