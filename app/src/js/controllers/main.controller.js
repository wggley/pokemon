/**
 * Main Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('mainController', mainController);

    mainController.$inject = ['$scope', '$state', 'mainService'];

    /**
     * Main Controller function
     * @param  {object} mainService Main Service object
     * @param  {object} $state angular $state object
     */
    function mainController($scope, $state, mainService) {
        var vm = this;

        //public functions
        vm.getLoadingStatus = getLoadingStatus;

        ///////

        /**
         * Listen on event goBack
         */
        $scope.$on('goBack', function() {
            goBack();
        });

        /**
         * Get loading Status
         * @return {boolean}
         */
        function getLoadingStatus() {
            return mainService.getLoadingStatus();
        }

        /**
         * Go back
         */
        function goBack() {
            if ($state.params.backState == "") {
                return $state.go('pokedex');
            }
            return $state.go($state.params.backState, $state.params.backStateParams);
        }
    };

})();
