/**
 * Comment Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('commentController', commentController);

    commentController.$inject = ['$scope', '$state', 'commentService'];

    /**
     * Comment Controller function
     * @param  {object} $scope          angular $scope object
     * @param  {object} $state          angular $state object
     * @param  {object} commentService Comment Service
     */
    function commentController($scope, $state, commentService) {
        var vm = this;
        vm.editing = false;
        vm.nationalId = $state.params.national_id;
        vm.id = $state.params.id;

        /*public functions*/
        vm.createComment = createComment;
        vm.updateComment = updateComment;
        vm.cancelCreateComment = cancelCreateComment;
        vm.submitForm = submitForm;
        vm.disableSubmitButton = disableSubmitButton;

        // controller init
        getComment();

        ///////

        /**
         * Cancel comment edition
         */
        function cancelCreateComment() {
            $scope.$emit('goBack');
        }

        /**
         * Create comment
         * @param  {string} name
         * @param  {string} email
         * @param  {string} description
         * @param  {boolean} valid       Form validation status
         */
        function createComment(name, email, description, valid) {
            if (formValidation(valid)) {
                commentService.createComment(cancelCreateComment, vm.nationalId, name, email, description);
            }
        }

        /**
         * Disable submit button
         * @param  {object} commentForm angular form
         * @return {boolean}
         */
        function disableSubmitButton(commentForm) {
            return commentForm.$pristine || (commentForm.$dirty && commentForm.$invalid);
        }

        /**
         * Form Validation
         * @param  {boolean} valid form validation status
         * @return {boolean}       true = ok, false = errors
         */
        function formValidation(valid) {
            if (!valid || vm.newComment.name == "" || vm.newComment.email == "" || vm.newComment.description == "") {
                return false;
            }
            return true;
        }

        /**
         * Get Comment
         */
        function getComment() {
            vm.newComment = {
                name: "",
                email: "",
                description: ""
            };

            var user = Parse.User.current();

            if (user) {
                vm.newComment.name = user.get('username');
                vm.newComment.email = user.get('email');
            }

            if (typeof(vm.nationalId) == "undefined" || vm.nationalId == "") {
                cancelCreateComment();
            }

            if (typeof(vm.id) == "string" && vm.id != "") {
                vm.editing = true;
                commentService.getComment(renderComment, vm.id);
            }
        }

        /**
         * Render Comment
         * @param  {object} comment Comment Service comment object
         */
        function renderComment(comment) {
            vm.comment = comment;
            if (typeof(comment) == "undefined") {
                return cancelCreateComment();
            }

            vm.newComment = {
                name: comment.get('name'),
                email: comment.get('email'),
                description: comment.get('description')
            };
            $scope.$apply();
        }

        /**
         * Submit Comment Form
         * @param  {object} commentForm angular form
         */
        function submitForm(commentForm) {
            if (vm.editing) {
                return vm.updateComment(vm.comment, vm.newComment.description, commentForm.$valid);
            }
            return vm.createComment(vm.newComment.name, vm.newComment.email, vm.newComment.description, commentForm.$valid);
        }

        /**
         * Update Comment
         * @param  {object} comment Comment Service comment object
         * @param  {string} description [description]
         * @param  {boolean} valid       form validation status
         */
        function updateComment(comment, description, valid) {
            if (formValidation(valid)) {
                commentService.updateComment(cancelCreateComment, comment, description);
            }
        }
    };
})();
