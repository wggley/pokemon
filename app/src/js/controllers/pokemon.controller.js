/**
 * Pokemon Controller
 */
(function() {
    angular
        .module('pokedexApp')
        .controller('pokemonController', pokemonController);

    pokemonController.$inject = ['$scope', '$state', 'pokedexService', 'pokemonService'];

    /**
     * Pokemon Controller Function
     * @param  {object} $scope         angular $scope object
     * @param  {object} $state         angular $state object
     * @param  {object} pokedexService Pokedex Service
     * @param  {object} pokemonService Pokemon Service
     */
    function pokemonController($scope, $state, pokedexService, pokemonService) {
        var vm = this;
        vm.pokemon = {};
        vm.previousButtonDisabled = false;
        vm.nextButtonDisabled = false;
        vm.nationalId = Number($state.params.national_id);

        //Public functions
        vm.getPokemonData = getPokemonData;
        vm.backToListing = backToListing;
        vm.previousPokemon = previousPokemon;
        vm.nextPokemon = nextPokemon;
        vm.showComments = showComments;

        //Init Controller
        getPokemonData();

        ///////

        /**
         * Back to Pokedex List
         */
        function backToListing() {
            $state.go('pokedex');
        }

        /**
         * Get Pokemon Data
         */
        function getPokemonData() {
            var pokedexData = pokedexService.getPokedexData();
            if (typeof(pokedexData) == "undefined") {
                pokedexService.getPokedex(function(data) {
                    var pokedexData = data;
                    var pokemon = pokedexService.getPokemonByNationalId(vm.nationalId);
                    if (typeof(pokemon) == "undefined") {
                        return backToListing();
                    }
                    pokemonService.setPokemon(pokemon, pokemon.index);
                    getPokemonData();
                });
                return;
            }
            pokemonService.getPokemonData(renderPokemon);
        }

        /**
         * Get Previous Pokemon
         */
        function previousPokemon() {
            vm.nextButtonDisabled = false;
            if (typeof(vm.pokemon.previousPokemon) == "undefined") {
                vm.previousButtonDisabled = true;
                return;
            }

            pokemonService.setPokemon(vm.pokemon.previousPokemon, vm.pokemon.index - 1);
            $state.go('pokemon', {
                national_id: vm.pokemon.previousPokemon.national_id
            });
        }

        /**
         * Get Next Pokemon
         */
        function nextPokemon() {
            vm.previousButtonDisabled = false;

            if (typeof(vm.pokemon.nextPokemon) == "undefined") {
                vm.nextButtonDisabled = true;
                return;
            }
            pokemonService.setPokemon(vm.pokemon.nextPokemon, vm.pokemon.index + 1);
            $state.go('pokemon', {
                national_id: vm.pokemon.nextPokemon.national_id
            });
        }

        /**
         * Render Pokemon Data
         * @param  {object} data Pokemon Data
         */
        function renderPokemon(data) {
            vm.pokemon = data;
        }

        /**
         * Show Comments
         */
        function showComments() {
            $state.go('comments', {
                national_id: vm.nationalId
            });
        }

    };

})();
