/**
 * Comments Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('commentService', commentService);

    commentService.$inject = ['$http', 'mainService'];

    /**
     * Comments Service function
     * @param  {object} $http angular $http object
     */
    function commentService($http, mainService) {
        //public functions
        var publicFunctions = {
            'getComment': getComment,
            'createComment': createComment,
            'updateComment': updateComment,
            'removeComment': removeComment
        };

        ///////


        /**
         * Get Comment by Id
         * @param  {function} promiseCallback function to run after success
         * @param  {string} id              comment Parse id
         */
        function getComment(promiseCallback, id) {
            mainService.setLoadingStatus(true);
            var Comment = Parse.Object.extend("Comment");
            var query = new Parse.Query(Comment);

            query.get(id)
                .then(function(result) {
                    console.log("Successfully retrieved " + result + " comments.");
                    return result;
                }, function(error) {
                    console.log("Error retrieving comments: " + error.code + " " + error.message);
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }

        /**
         * Create comment
         * @param  {function} promiseCallback function to run after success
         * @param  {Number} nationalId      Pokedex National Id
         * @param  {string} name
         * @param  {string} email
         * @param  {string} description
         */
        function createComment(promiseCallback, nationalId, name, email, description) {
            mainService.setLoadingStatus(true);
            var Comment = Parse.Object.extend("Comment");
            var comment = new Comment();
            var user = Parse.User.current();

            comment.set("nationalId", Number(nationalId));
            comment.set("name", name);
            comment.set("email", email);
            comment.set("description", description);
            comment.set("date_created", new Date());
            comment.set("user", user);

            comment.save()
                .then(function(comment) {
                    console.log('New comment created with objectId: ' + comment.id);
                    return comment;
                }, function(error) {
                    console.log('Failed to create new comment, with error code: ' + error.message);
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }


        /**
         * Update comment
         * @param  {function} promiseCallback function to run after success
         * @param  {object} comment      Parse comment
         * @param  {string} description
         */
        function updateComment(promiseCallback, comment, description) {
            mainService.setLoadingStatus(true);
            var user = Parse.User.current();
            comment.set("description", description);
            comment.set("date_updated", new Date());
            comment.set("user", user);

            comment.save()
                .then(function(comment) {
                    console.log('comment updated with objectId: ' + comment.id);
                    return comment;
                }, function(error) {
                    console.log('Failed to update comment, with error code: ' + error.message);
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }

        /**
         * Remove comment
         * @param  {function} promiseCallback function to run after success
         * @param  {object} comment      Parse comment
         */
        function removeComment(promiseCallback, comment) {
            mainService.setLoadingStatus(true);
            comment.destroy()
                .then(function(comment) {
                        console.log('comment removed');
                        return comment;
                    },
                    function(error) {
                        console.log('Failed to remove comment, with error code: ' + comment.message);
                    })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });

        }

        return publicFunctions;
    }
})();
