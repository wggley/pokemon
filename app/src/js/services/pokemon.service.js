/**
 * Pokemon Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('pokemonService', pokemonService);

    pokemonService.$inject = ['$http', 'mainService', 'pokedexService'];

    /**
     * Pokemon Service function
     * @param  {object} $http angular $http object
     * @param  {object} mainService Service
     * @return {object}       public functions
     */
    function pokemonService($http, mainService, pokedexService) {
        var baseUrl = 'http://pokeapi.co/';
        var defaultImage = "assets/img/loading-pokeball.gif";
        var regExp = /^\//;
        var pokemon;

        //public functions
        var publicFunctions = {
            'getPokemonData': getPokemonData,
            'setPokemon': setPokemon
        };

        ///////

        /**
         * Get Pokemon
         * @return {object} pokemon data from pokedex API
         */
        function getPokemon() {
            return pokemon;
        }

        /**
         * Get Pokemon Data
         * @param  {function} promiseCallback function to run after success
         */
        function getPokemonData(promiseCallback) {
            mainService.setLoadingStatus(true);
            var pokemon = getPokemon();
            var resourceUri = pokemon['resource_uri'];

            return $http.get(baseUrl + resourceUri.replace(regExp, ''), {
                    cache: true
                })
                // get description data
                .then(function(response) {
                    pokemon.description = "no description yet";
                    pokemon.image = defaultImage;

                    pokemon.info = response.data;
                    if (pokemon.info.descriptions.length > 0) {
                        var resourceUri = pokemon.info.descriptions[0]['resource_uri'];
                        return $http.get(baseUrl + resourceUri.replace(regExp, ''), {
                            cache: true
                        })
                    }
                })
                //get sprite data
                .then(function(response) {
                    if (typeof(response) != 'undefined') {
                        pokemon.description = response.data.description;
                    }
                    if (pokemon.info.sprites.length > 0) {
                        var resourceUri = pokemon.info.sprites[0]['resource_uri'];
                        return $http.get(baseUrl + resourceUri.replace(regExp, ''), {
                            cache: true
                        })
                    }
                })
                //set data and run callback
                .then(function(response) {
                    if (typeof(response) != "undefined") {
                        pokemon.image = 'http://pokeapi.co' + response.data.image;
                    }
                    promiseCallback(pokemon);
                }, function(response) {
                    console.log('DEBUG: error acessing data');
                })
                .finally(function() {
                    mainService.setLoadingStatus(false);
                });
        }

        /**
         * Set Pokemon Data from Pokedex Api
         * @param {object} pokemonData from Pokedex Api
         * @param {Number} index from Pokedex Api
         */
        function setPokemon(pokemonData, index) {
            pokemon = pokemonData;
            pokemon.index = index;
            pokemon.previousPokemon = undefined;
            var pokedexData = pokedexService.getPokedexData();
            if (pokemon.index > 0) {
                pokemon.previousPokemon = pokedexData[index - 1];
            }
            pokemon.nextPokemon = undefined;
            if (pokemon.index < pokedexData.length - 1) {
                pokemon.nextPokemon = pokedexData[index + 1];
            }
        }

        return publicFunctions;
    }
})();
