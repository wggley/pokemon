/**
 * Main Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('mainService', mainService);

    mainService.$inject = ['$http'];
    /**
     * Main Service Function
     * @param  {object}
     * @return {object}
     */
    function mainService($http) {
        var callLoading = 0;
        var isLoading = false;

        var publicFunctions = {
            'getData': getData,
            'getLoadingStatus': getLoadingStatus,
            'setLoadingStatus': setLoadingStatus
        };

        /**
         * Generic $http get and store data
         * @param  {string} url             url used to access REST api
         * @param  {function} promiseCallback function to run after success
         */
        function getData(url, promiseCallback) {
            setLoadingStatus(true);

            return $http.get(url, {
                    cache: true
                })
                .then(function(response) {
                    promiseCallback(response.data);
                }, function(response) {
                    console.log('DEBUG: error acessing data from url: ' + url);
                })
                .finally(function() {
                    setLoadingStatus(false);
                });
        }

        /**
         * Get loading status
         * @return {boolean}
         */
        function getLoadingStatus() {
            return isLoading;
        }

        /**
         * Set loading status
         * @param {boolean}
         */
        function setLoadingStatus(status) {
            if (status) {
                callLoading++;
                isLoading = true;
            } else {
                callLoading--;
                if (callLoading == 0) {
                    isLoading = false;
                }
            }
        }

        return publicFunctions;
    }
})();
