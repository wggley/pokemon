/**
 * User Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('userService', userService);

    userService.$inject = ['$http', 'mainService'];

    /**
     * User Service function
     * @param  {object} $http angular $http object
     */
    function userService($http, mainService) {
        //public functions
        var publicFunctions = {
            'getUser': getUser,
            'createUser': createUser,
            'updateUser': updateUser,
            'removeUser': removeUser
        };

        ///////


        /**
         * Get User by Id
         * @param  {function} promiseCallback function to run after success
         * @param  {string} name   user name
         * @param  {string} password  user password
         */
        function getUser(promiseCallback, name, password) {
            mainService.setLoadingStatus(true);

            Parse.User.logIn(name, password)
                .then(function(result) {
                    console.log("Successfully retrieved " + result + " User.");
                    return result;
                }, function(error) {
                    console.log("Error retrieving User: " + error.code + " " + error.message);
                    return error.message;
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }

        /**
         * Create user
         * @param  {function} promiseCallback function to run after success
         * @param  {string} name
         * @param  {string} email
         * @param  {string} password
         */
        function createUser(promiseCallback, name, email, password) {
            mainService.setLoadingStatus(true);
            var user = new Parse.User();

            user.set("email", email);
            user.set("username", name);
            user.set("password", password);
            user.set("date_created", new Date());

            user.signUp()
                .then(function(user) {
                    console.log('New user created with objectId: ' + user.id);
                    return user;
                }, function(error) {
                    console.log('Failed to create new user, with error code: ' + error.message);
                    return error.message;
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }


        /**
         * Update user
         * @param  {function} promiseCallback function to run after success
         * @param  {object} email
         * @param  {string} password
         */
        function updateUser(promiseCallback, email, password) {
            mainService.setLoadingStatus(true);
            var user = Parse.User.current();
            user.set("email", email);
            user.set("password", password);
            user.set("date_updated", new Date());

            user.save()
                .then(function(user) {
                    console.log('user updated with objectId: ' + user.id);
                    return user;
                }, function(error) {
                    console.log('Failed to update user, with error code: ' + error.message);
                    return error.message;
                })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });
        }

        /**
         * Remove user
         * @param  {function} promiseCallback function to run after success
         * @param  {object} user      Parse user
         */
        function removeUser(promiseCallback, user) {
            mainService.setLoadingStatus(true);
            user.destroy()
                .then(function(user) {
                        console.log('user removed');
                        return user;
                    },
                    function(error) {
                        console.log('Failed to remove user, with error code: ' + user.message);
                        return error.message;
                    })
                .always(function(result) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(result);
                });

        }

        return publicFunctions;
    }
})();
