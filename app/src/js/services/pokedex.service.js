/**
 * Pokedex Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('pokedexService', pokedexService);

    pokedexService.$inject = ['$http', 'mainService'];

    /**
     * PokedexService function
     * @param  {object}
     * @param  {object}
     * @return {object}
     */
    function pokedexService($http, mainService) {
        var baseUrl = 'http://pokeapi.co/';
        var regExp = /^\//;
        var pokedexData;

        var publicFunctions = {
            'getNationalId': getNationalId,
            'getPokedex': getPokedex,
            'getPokedexData': getPokedexData,
            'getPokemonByNationalId': getPokemonByNationalId
        };

        ///////


        /**
         * Get Pokemon National Id
         * @param  {string} resourceUri uri used to access REST api
         * @return {Number}             National Pokedex Id
         */
        function getNationalId(resourceUri) {
            return Number(resourceUri.replace(/.*\/([0-9].*)\/$/, '$1'));
        }

        /**
         * Get National Pokedex Information
         * @param  {function} promiseCallback function to run after success
         */
        function getPokedex(promiseCallback) {
            mainService.setLoadingStatus(true);
            var callback = function(data) {
                //sorting pokedex api data by national_id
                data.pokemon.sort(function(a, b) {
                    var x = getNationalId(a['resource_uri']);
                    var y = getNationalId(b['resource_uri']);
                    a['national_id'] = x;
                    b['national_id'] = y;
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });

                pokedexData = data.pokemon;
                mainService.setLoadingStatus(false);
                promiseCallback(pokedexData);
            }
            mainService.getData(baseUrl + 'api/v1/pokedex/1/', callback);
        }

        /**
         * Get Pokedex Data
         * @return {object} pokedex Data
         */
        function getPokedexData() {
            return pokedexData;
        }

        /**
         * Get Pokemon by National Id info
         * @param  {Number} nationalId Pokedex National Id
         * @return {object}            Pokemon Data
         */
        function getPokemonByNationalId(nationalId) {
            for (var i = 0; i < pokedexData.length; i++) {
                if (pokedexData[i].national_id == nationalId) {
                    var pokemon = pokedexData[i];
                    pokemon.index = i;
                    return pokemon;
                }
            };
        }

        return publicFunctions;
    }
})();
