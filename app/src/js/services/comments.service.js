/**
 * Comments Service
 */
(function() {
    angular
        .module('pokedexApp')
        .factory('commentsService', commentsService);

    commentsService.$inject = ['$http', 'mainService'];

    /**
     * Comments Service function
     * @param  {object} $http angular $http object
     */
    function commentsService($http, mainService) {
        //public functions
        var publicFunctions = {
            'getComments': getComments
        };

        ///////

        /**
         * Get Pokemon Comments
         * @param  {function} promiseCallback function to run after success
         * @param  {Number} nationalId      Pokedex National Id
         */
        function getComments(promiseCallback, nationalId) {
            mainService.setLoadingStatus(true);
            var Comment = Parse.Object.extend("Comment");
            var query = new Parse.Query(Comment);
            return query
                .equalTo("nationalId", Number(nationalId))
                .find()
                .then(function(results) {
                    console.log("Successfully retrieved " + results.length + " comments.");
                    for (var i = 0; i < results.length; i++) {
                        results[i]['date_created'] = results[i].get('date_created');
                    };
                    return results;
                }, function(error) {
                    console.log("Error retrieving comments: " + error.code + " " + error.message);
                })
                .always(function(results) {
                    mainService.setLoadingStatus(false);
                    promiseCallback(results);
                });
        }

        return publicFunctions;
    }
})();
