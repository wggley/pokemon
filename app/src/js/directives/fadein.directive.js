/*Animate img on load*/
(function() {
    angular
        .module('pokedexApp')
        .directive('fadeIn', fadeIn);

    fadeIn.$inject = ['$timeout'];

    function fadeIn($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element, attrs) {
                $element.addClass("ng-hide-remove");
                $element.on('load', function() {
                    $element.addClass("ng-hide-add");
                });
                $scope.$watch('pokemon.image', function(newVal, oldVal) {
                    if (newVal !== oldVal) {
                        $element.attr('srcset', 'assets/img/loading-pokeball.gif');
                        $element.removeClass("ng-hide-add");
                        $element.attr('srcset', newVal);
                    }
                });
            }
        }
    }
})();
