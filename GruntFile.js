/*!
 * Pokemon Gruntfile
 * @author Wallace Goulart Gaudie Ley
 */

'use strict';

/**
 * Grunt Module
 */
module.exports = function(grunt) {
    /**
     * Load Grunt plugins
     */
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    /**
     * Configuration
     */
    grunt.initConfig({
        /**
         * Get package meta data
         */
        pkg: grunt.file.readJSON('package.json'),

        /**
         * Set project object
         */
        project: {
            app: 'app',
            assets: '<%= project.app %>/assets',
            src: '<%= project.app %>/src',
            dest: '<%= project.app %>/dest',
            prod: 'public',
            css: [
                '<%= project.src %>/scss/imports.scss'
            ],
            js: [
                '<%= project.src %>/js/*.js'
            ]
        },


        /**
         * Sass
         */
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    compass: true,
                    sourcemap: 'none'
                },
                files: {
                    '<%= project.assets %>/css/styles.css': '<%= project.css %>'
                }
            },
            dist: {
                options: {
                    style: 'compressed',
                    compass: true,
                    sourcemap: 'none'
                },
                files: {
                    '<%= project.dest %>/assets/css/styles.min.css': '<%= project.dest %>/assets/css/styles.css'
                }
            }
        },

        /**
         * Watch
         */
        watch: {
            sass: {
                files: '<%= project.src %>/scss/{,*/}*.{scss,sass}',
                tasks: ['sass:dev']
            }
        },

        /**
         * Uglify
         */
        uglify: {
            options: {
                report: 'min',
                mangle: true
            },
            dist: {
                files: {
                    "<%= project.dest %>/src/js/script.js": [
                        "<%= project.dest %>/src/js/app.js",
                        "<%= project.dest %>/src/js/states.js",
                        "<%= project.dest %>/src/js/controllers/*.js",
                        "<%= project.dest %>/src/js/filters/*.js",
                        "<%= project.dest %>/src/js/directives/*.js",
                        "<%= project.dest %>/src/js/services/*.js"
                    ]
                }
            }
        },

        /*Process HTML*/
        processhtml: {
            dist: {
                files: {
                    '<%= project.dest %>/index.html': ['<%= project.dest %>/index.html']
                }
            }
        },


        /*concat js files*/
        concat: {
            dist: {
                files: {
                    "<%= project.dest %>/src/js/script.min.js": [
                        "<%= project.dest %>/src/js/libs/angular.min.js",
                        "<%= project.dest %>/src/js/libs/angular-animate.min.js",
                        "<%= project.dest %>/src/js/libs/angular-sanitize.min.js",
                        "<%= project.dest %>/src/js/libs/angular-spinners.min.js",
                        "<%= project.dest %>/src/js/libs/angular-ui-router.min.js",
                        "<%= project.dest %>/src/js/libs/detectmobilebrowser.min.js",
                        "<%= project.dest %>/src/js/libs/parse-1.6.0.min.js",
                        "<%= project.dest %>/src/js/script.js"
                    ]
                }
                /*src: [
                    "<%= project.dest %>/src/js/libs/*.js",
                    "<%= project.dest %>/src/js/script.js"
                ],
                dest: "<%= project.dest %>/src/js/script.min.js"*/
            }
        },

        /*copy assets*/
        copy: {
            dist: {
                files: [{
                    cwd: '<%= project.app %>',
                    expand: true,
                    src: [
                        "index.html",
                        "src/js/**",
                        "src/views/**",
                        "assets/**"
                    ],
                    dest: "<%= project.dest %>"
                }]
            },
            prod: {
                files: [{
                    cwd: '<%= project.dest %>',
                    expand: true,
                    src: [
                        "index.html",
                        "src/js/**",
                        "src/views/**",
                        "assets/**"
                    ],
                    dest: "<%= project.prod %>"
                }]
            }
        },

        /*clean files*/
        clean: {
            dist: ['<%= project.dest %>/*'],
            prod: ['<%= project.prod %>/*']
        },

        /*Automated testing*/
        protractor: {
            options: {
                configFile: "test/dev.conf.js", // Default config file
                keepAlive: false, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                args: {} // Target-specific arguments
            },
            dev: {
                options: {
                    configFile: "test/dev.conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            },
            dist: { // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: "test/dest.conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            },
            prod: { // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: "test/prod.conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            },
        },

        /*execute commands on terminal*/
        exec: {
            deploy: 'parse deploy',
            webdriver_manager: 'webdriver-manager start'
        }

    });


    /**
     * Default task
     * Run `grunt` on the command line
     */
    grunt.registerTask('default', [
        'sass:dev',
        'watch'
    ]);

    grunt.registerTask('w', [
        'watch'
    ]);

    grunt.registerTask('s', [
        'sass:dev'
    ]);

    grunt.registerTask('t', [
        'exec:webdriver_manager'
    ]);

    grunt.registerTask('b', [
        'clean:dist',
        'copy:dist',
        'sass:dist',
        'uglify',
        'concat',
        'processhtml',
        'protractor:dist',
        'clean:prod',
        'copy:prod',
        'clean:dist',
    ]);

    grunt.registerTask('d', [
        'exec:deploy',
        'protractor:prod',
        'clean:prod'
    ]);
};
