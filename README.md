# Summary
Basic [Pokedex](http://pokedex.parseapp.com) featuring: 

* Responsive Design. 

* [REST API](http://pokeapi.co). 

* [Parse](http://parse.com) Cloud Storage. 

* Excellent [Pokemon GB @font-face](http://pt.fonts2u.com/pokemon-gb.fonte). 

* [Grunt](http://gruntjs.com/) and [Protractor](https://angular.github.io/protractor/#/) for testing.


## Getting Started
This projects requires Grunt `>=0.4.5` and a Webserver to run (Apache, Nginx, IIS, choose one you like :) )

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this project with this command:

```shell
npm install

```

Once the dependencies are installed you need to run [Protractor](https://angular.github.io/protractor/#/) Selenium instance for testing purposes with command:
```shell
grunt t
```
If you have issues regarding running Protractor on Windows (node-gyp errors) follow this [tutorial](https://github.com/nodejs/node-gyp/wiki/Visual-Studio-2010-Setup).

## Build
You can build with the following command:

```shell
grunt b
```
It will test your environment and install at `/public` folder.

## Deploy
You can deploy to a [Parse](http://parse.com) account using command:

```shell
grunt d
```
But first you need to configure [Parse](http://parse.com) account keys so deployment can be [automated](https://parse.com/docs/js/guide#command-line-account-keys)

## Author
wggley:

* [Linkedin](https://www.linkedin.com/pub/wallace-goulart-gaudie-ley/7/66b/23b)

* [Github](https://github.com/wggley)

* [Bitbucket](https://bitbucket.org/wggley)